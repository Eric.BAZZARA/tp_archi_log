
import java.util.ArrayList;
import java.util.Collections;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;


public class Main
{
    public static void main(String[] args)
    { 
        //System.out.println("ca compile");
        Carte carte = new Carte(300, 300, 20000);
        Simulateur simulateur = new Simulateur(carte);

        carte.alea.setTypeGenerateur(GenerateurAleatoire.JAVA_UTIL);
        simulateur.lancerReplications(100, 730, "resultats_simulations/java");

        /*carte.alea.setTypeGenerateur(GenerateurAleatoire.MERSENNE_TWISTER);
        simulateur.lancerReplications(100, 730, "resultats_simulations/mersenne");*/

        /*carte.alea.setTypeGenerateur(GenerateurAleatoire.MAUVAIS_LCG);
        simulateur.lancerReplications(100, 730, "resultats_simulations/lcg");*/
    }
}

enum Etat
{
    SUSCEPTIBLE, EXPOSE, INFECTIEUX, IMMUNISE
}


class Individu
{   
    // Durées des états dans lesquels un individu reste avant de passer
    // à un autre état. Le seul état qui n'a pas de limite est l'état
    // SUSCEPTIBLE qui correspond à l'état de base.
    public int dE; // Durée d'exposition
    public int dR; // Durée d'immunité
    public int dI; // Durée d'infection

    public int x;         // Position de l'individu
    public int y;         // Position de l'individu
    public Etat etat;     // Etat de l'individu
    public int dureeEtat; //Durée pendant laquelle l'individu est resté dans un état

    Individu()
    {   
        etat = Etat.SUSCEPTIBLE;
        dureeEtat = 0;
        dE = 2; 
        dR = 2; 
        dI = 2;
    }

    /**
     * Met à jour l'état de l'individu et réinitialise le compteur de durée
     * @param etat : nouvel état à donner à l'individu
     */
    public void setNouvelEtat(Etat etat)
    {
        this.etat = etat;
        dureeEtat = 0; 
    }

    /**
     * Augmente la durée passée dans l'état actuel de 1
     */
    public void incrementerDureeEtat()
    {
        dureeEtat++;
    }

}



class Carte
{
    private int 
        longueur,        // longueur de la carte
        largeur,         // largeur de la carte
        nb_individus,    // nombre d'individus présents sur la carte

        nb_susceptibles, // nombre de susceptibles parmis tous les individus
        nb_exposes,      // nombre d'exposés parmis tous les individus
        nb_infectieux,   // nombre d'infectieux parmis tous les individus
        nb_immunises;    // nombre d'immunisés parmis tous les individus


    private ArrayList<Individu> listeIndividus; // liste de tous les individus
    private int[][] tableauInfectes;            // tableau comptant le nombre d'infectés par position

    public GenerateurAleatoire alea;           // Générateur de nombres aléatoires


    Carte(int longueur, int largeur, int nb_individus)
    {
        this.longueur     = longueur;
        this.largeur      = largeur;
        this.nb_individus = nb_individus;

        // Création de la liste d'individus
        listeIndividus = new ArrayList<Individu>(this.nb_individus);

        for (int i = 0; i < this.nb_individus; i++) listeIndividus.add(new Individu());
        
        // Création du tableau comptant le nombre d'infectés par position
        tableauInfectes = new int[this.longueur][this.largeur];

        // Instanciation du générateur de nombres aléatoires
        alea = new GenerateurAleatoire(GenerateurAleatoire.MERSENNE_TWISTER);

        // Initialisation des données
        initialiserIndividus();
        
    }


    public int getNbSusceptibles(){
        return nb_susceptibles;
    }

    public int getNbExposes() {
        return nb_exposes;
    }

    public int getNbInfectieux(){
        return nb_infectieux;
    }

    public int getNbImmunises(){
        return nb_immunises;
    }


    /**
     * Donne une valeur initiale à tous les attributs des individus
     */
    public void initialiserIndividus()
    {   
        nb_susceptibles = nb_individus - 20;
        nb_exposes      = 0;
        nb_infectieux   = 20;
        nb_immunises    = 0;

        for (int i = 0; i < this.longueur; i++)
        {
            for (int j = 0; j < this.largeur; j++) tableauInfectes[i][j] = 0;
        }

        int compt = 0;
        for (Individu individu : listeIndividus) 
        {
            individu.x = alea.entierEntre(0, longueur);
            individu.y = alea.entierEntre(0, largeur);

            if (compt < nb_infectieux)
            {                              
                individu.etat = Etat.INFECTIEUX;
                tableauInfectes[individu.x][individu.y]++;  
                compt++;
            }
            else
            {
                individu.etat = Etat.SUSCEPTIBLE;
            }
            individu.dE = (int) alea.loiExponentielle(3);
            individu.dI = (int) alea.loiExponentielle(7);
            individu.dR = (int) alea.loiExponentielle(365);
        }
    }


    /**
     * Détermine si un individu est exposé avec une certaine probabilité
     * @param nb_infectes : nombre de personnes infectés dans le voisinage
     * de l'individu
     * @return un bouléen qui indique si l'individu est infecté ou pas
     */
    private boolean estExpose(int nb_infectes)
    {
        boolean res = true;

        double proba_infection = 1.0 - Math.exp(-0.5 * (double) nb_infectes);
        double tirage = alea.reelEntre(0.0, 1.0);

        if (tirage > proba_infection) res = false;

        return res;
    }


    /**
     * Renvoie le nombre d'individus infectieux dans un voisinage donné
     * @param x, y : position dont on veut examiner le voisinage
     * @return le nombre d'individus infectieux autour de (x, y)
     */
    private int nbVoisinsInfectieux(int x, int y)
    {
        int x_voisin, y_voisin,
            nbInfectieux = 0;

        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                x_voisin = (x + i + longueur) % longueur; // pas testé si un modulo du type
                y_voisin = (y + j + largeur)  % largeur;  // -1%6 marche et donne 5

                nbInfectieux += tableauInfectes[x_voisin][y_voisin];
            }
        }

        return nbInfectieux;
    }


    /**
     * Effectue les actions d'un individu au cours d'un pas de temps
     * @param individu : index de l'individu 
     */
    private void traiterIndividu(Individu individu)
    {   
        int nb_voisins; // Nombre de voisins infectieux

        if (individu.etat == Etat.INFECTIEUX){
            tableauInfectes[individu.x][individu.y]--;
        }

        individu.x = alea.entierEntre(0, longueur);
        individu.y = alea.entierEntre(0, largeur);


        switch (individu.etat)
        {
            case SUSCEPTIBLE:
                
                nb_voisins = nbVoisinsInfectieux(individu.x, individu.y);

                if (estExpose(nb_voisins))
                {
                    individu.setNouvelEtat(Etat.EXPOSE);
                    nb_susceptibles--;
                    nb_exposes++;
                }
                break;
            
            case EXPOSE:

                if (individu.dureeEtat > individu.dE)
                {
                    individu.setNouvelEtat(Etat.INFECTIEUX);
                    tableauInfectes[individu.x][individu.y]++;
                    nb_exposes--;
                    nb_infectieux++;
                }
                break;

            case INFECTIEUX:

                tableauInfectes[individu.x][individu.y]++;

                if (individu.dureeEtat > individu.dI)
                {
                    individu.setNouvelEtat(Etat.IMMUNISE);
                    tableauInfectes[individu.x][individu.y]--;
                    nb_infectieux--;
                    nb_immunises++;
                }
                break;

            case IMMUNISE:

                if (individu.dureeEtat > individu.dR){
                    individu.setNouvelEtat(Etat.SUSCEPTIBLE);
                    nb_immunises--;
                    nb_susceptibles++;
                }
                break;
        }

        individu.incrementerDureeEtat();
    }


    /**
     * Traite tous les individus dans le cadre d'un pas de temps
     */
    public void traiterPopulation()
    {
        Collections.shuffle(listeIndividus);

        for (Individu individu : listeIndividus) traiterIndividu(individu);
    }
}



class Simulateur
{
    private Carte carte;

    Simulateur(Carte carte)
    {
        this.carte = carte;
    }

    /**
     * Lance une simulation entière
     * @param nb_tours : Nombre de tours à effectuer dans une simulation
     * @param nom_fichier : adresse du fichier dans lequel inscire les résultats
     */
    public void lancerSimulation(int nb_tours, String nom_fichier)
    {
        carte.initialiserIndividus();


        PrintStream stream = ouvrirFichier(nom_fichier);
        stream.println("Pas;Susceptibles;Exposés;Infectieux;Immunisés");
        ecrireEtatSimulationDansFichier(stream, 0);

        for (int j = 1; j <= nb_tours; j++)
        {   
            carte.traiterPopulation();
            ecrireEtatSimulationDansFichier(stream, j);
    
        }

        stream.close(); 
        stream = null;
    }


    /**
     * Lance des réplications de la simulation
     * @param nb_replications : nombre de réplications à faire
     * @param nb_tours : nombre de tours à faire dans une simulation
     * @param dossier : chemin du dossier dans lequel stocker les 
     * résultats (doit exister)
     */
    public void lancerReplications(int nb_replications, int nb_tours, String dossier)
    {
        String fichierSortie = dossier + "/replication_%d.csv";

        System.out.print("Simulations : ");
        for (int i = 1; i <= nb_replications; i++)
        {   
            System.out.print(i + "  ");
            //System.out.println("Fichier de sortie : " + String.format(fichierSortie, i));
            lancerSimulation(nb_tours, String.format(fichierSortie, i));
        }
        System.out.println("");
    }

    /**
     * Ecrit dans un fichier l'état de la simulation (i.e le nombre de d'exposés, 
     * de susceptibles, etc.) dans un format .csv
     * @param stream : fichier ouvert dans lequel on peut écrire
     * @param etape : numéro du pas de temps
     * @param carte
     */
    private void ecrireEtatSimulationDansFichier(PrintStream stream, int etape)
    {
        String patron = "%d; %d; %d; %d; %d\n";

        stream.printf(patron, etape, 
                              carte.getNbSusceptibles(), 
                              carte.getNbExposes(), 
                              carte.getNbInfectieux(), 
                              carte.getNbImmunises()
                      );

    }


    /**
     * Ouvre un fichier en le créant s'il n'existe pas
     * @param nomFichier : chemin du fichier à ouvrir
     * @return un objet permettant d'écrire dans le fichier
     */
    private PrintStream ouvrirFichier(String nomFichier)
    {   
        PrintStream stream = null;
        File fichier = new File(nomFichier);

        try
        {
            if (!fichier.exists()) fichier.createNewFile();

            stream = new PrintStream(new FileOutputStream(nomFichier, false));
        }
        catch (java.io.FileNotFoundException fne)
        {
            System.out.println("[ouvrirFichier] Erreur : Cette ligne ne devrait jamais s'afficher. Un pointeur null va etre renvoye.");
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("[ouvrirFichier] Erreur : Le fichier n'a pas pu etre creer. Un pointeur null va etre renvoye.");
        }
        
        return stream;
    }
}
