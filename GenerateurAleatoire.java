
import nedragtna.random.MTRandom;
import java.util.Random;

public class GenerateurAleatoire
{   
    // Liste des différentes façons de générer des nombres aléatoires
    public static final int JAVA_UTIL        = 0;
    public static final int MERSENNE_TWISTER = 1;
    public static final int MAUVAIS_LCG      = 2;

    private PrimitivesGenerateurAleatoire typeGenerateur;

    private PrimitivesGenerateurAleatoire javaUtil;
    private PrimitivesGenerateurAleatoire mersenneTwister;
    private PrimitivesGenerateurAleatoire petitLCG;

    GenerateurAleatoire()
    {   
        this(JAVA_UTIL);
    }

    GenerateurAleatoire(int generateur)
    {   
        javaUtil        = new GenerateurJavaUtil();
        mersenneTwister = new GenerateurMersenneTwister();
        petitLCG        = new GenerateurLCG();

        setTypeGenerateur(generateur);
    }

    public void setTypeGenerateur(int generateur)
    {   

        switch (generateur)
        {
            case JAVA_UTIL:
                typeGenerateur = javaUtil;
                break;

            case MERSENNE_TWISTER:
                typeGenerateur = mersenneTwister;
                break;

            case MAUVAIS_LCG:
                typeGenerateur = petitLCG;
                break;
            
            default:
                System.out.println("[GenerateurAleatoire.setTypeGenerateur] Warning : Generateur inconnu.");
                System.out.println("Choix du generateur par defaut.");
                typeGenerateur = javaUtil;
        }

    }

    /**
     * Génère un entier dans [a, b[ suivant une loi uniforme
     * @param a, b : intervalle [a, b[
     * @return un entier dans [a, b[
     */
    public int entierEntre(int a, int b)
    {
        return typeGenerateur.genererEntierEntre(a, b);
    }

    /**
     * Génère un réel dans [a, b[ suivant une loi uniforme
     * @param a, b : intervalle [a, b[
     * @return un réel dans [a, b[
     */
    public double reelEntre(double a, double b)
    {
        return (typeGenerateur.genererReelEntre0Et1() * (b - a)) + a;
    }

    /**
     * Génère un nombre suivant une loi exponentielle
     * @param moyenne : paramètre de loi qui permet d'obtenir cette valeur en moyenne
     * @return un nombre suivant une loi exponentielle
     */
    public double loiExponentielle(double moyenne)
    {
        return -moyenne * Math.log(1.0 - typeGenerateur.genererReelEntre0Et1());
    }
}


interface PrimitivesGenerateurAleatoire
{
    int genererEntierEntre(int a, int b);

    double genererReelEntre0Et1();
}


class GenerateurJavaUtil implements PrimitivesGenerateurAleatoire
{   
    private Random rand;

    GenerateurJavaUtil()
    {
        rand = new Random();
        rand.setSeed(123457789L);
    }
    
    public int genererEntierEntre(int a, int b)
    {
        return rand.nextInt(a, b);
    }

    public double genererReelEntre0Et1()
    {
        return rand.nextDouble();
    }
}


class GenerateurMersenneTwister implements PrimitivesGenerateurAleatoire
{
    private MTRandom rand;

    GenerateurMersenneTwister()
    {   
        int [] init = {0x123, 0x234, 0x345, 0x456};
        rand = new MTRandom(init);
    }

    public int genererEntierEntre(int a, int b)
    {
        return (int) ( rand.nextDouble() * ((double) (b - a)) ) + a;
    }

    public double genererReelEntre0Et1()
    {
        return rand.nextDouble();
    }
}


/*Implémentation d'un 'mauvais' générateur */
class GenerateurLCG implements PrimitivesGenerateurAleatoire
{
    private int suivant;

    GenerateurLCG()
    {   
        suivant = 123456;
    }

    private int calculerNext()
    {
        // Adapté du man 3 rand
        /* RAND_MAX assumed to be 32767 
          int myrand(void) {
           next = next * 1103515245 + 12345;
              return((unsigned)(next/65536) % 32768);
        }*/
        suivant = suivant * 1115245 + 12345;
        int res = ((suivant/65536) % 1000);//32768);

        if (res < 0 ) res = - res; // Why not après tout ?
        return res;
    }

    public int genererEntierEntre(int a, int b)
    {
        return  ( calculerNext() % (b - a) ) + a;
        // Si b - a est plus grand que le modulo, les dernières valeurs
        // ne seront jamais atteintes, mais bon ça n'a pas d'importance
        // vu l'utilisation qu'on va en faire
    }

    public double genererReelEntre0Et1()
    {
        return (double) calculerNext() / 1000.0;// 32768.0;
    }
}

      
