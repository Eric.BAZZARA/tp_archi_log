# Nom des étudiants
- THOMAZET Rémi
- BAZZARA Eric

# PRNG
Il y a trois types de générateurs : 
- celui fournit par Java avec sa classe Random()
- le Mersenne Twister
- un LCG (assez mauvais avec une période maximale de 1000)

# Des remarques
- Le diagramme de classe est dans le fichier 'DiagrammeDeClasse.pdf' 
- Le notebook des résultats de simulations est le ficher 'resultats.html'
- Parmi toutes les réplications, il y en a une étrange dans le générateur de Java (18ème ligne, 3ème colonne)